from django.shortcuts import render
from django.shortcuts import redirect
from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm

from django import forms
from .forms import Cform, Cform1

from .models import Artes

# Create your views here.
def index (request):
    return render(request,"home/CRUD_list.html",{})


def create(request):
    form = Cform(request.POST or None)
    message="Ingresa"
    if request.user.is_authenticated:
        message ="Acceso"
        if form.is_valid():
           instance = form.save(commit=False)
           form.save()
           return redirect('/')

        else:
           error = "log in"
    context ={"form":form,"message":error}
    return render(request,"home/create.html",context)


def create1(request):
    form = Cform1(request.POST or None)
    message="Ingresa"
    if request.user.is_authenticated:
        message ="Acceso"
        if form.is_valid():
           instance = form.save(commit=False)
           form.save()
           return redirect('/')

        else:
           error = "log in"
    context ={"form":form,"message":error}
    return render(request,"home/create1.html",context)

def register(response):
    if response.method == "POST":
        form = UserCreationForm(response.POST)
        if form.is_valid():
            form.save()

        return redirect("/")
    else:
        form = UserCreationForm()
    return render(response,"home/register.html",{"form":form})

def list(request):
    Arte =Artes.objects.all()
    return render(request,"home/here2.html",{'Arte':Arte})

def detail22(request ,id):
    arte= Artes.objects.get(id=id)
    form =Cform(request.POST or None, instance=arte)
    if form.is_valid():
        form.save()
        return redirect('list')
    return render(request, "home/here3.html",{"form":form,"arte":arte})



def detail(request ,id):
    queryset= Artes.objects.get(modelo=id)
    context = {"object":queryset}
    return render (request,"home/artes_list.html",context)

def temp1 (request):
    return render(request,"home/1.html",{})
def temp2 (request):
    return render(request,"home/2.html",{})
def temp3 (request):
    return render(request,"home/3.html",{})
def temp4 (request):
    return render(request,"home/4.html",{})
def temp5 (request):
    return render(request,"home/5.html",{})
def temp6 (request):
    return render(request,"home/6.html",{})
def temp7 (request):
    return render(request,"home/7.html",{})
def temp8 (request):
    return render(request,"home/8.html",{})
def temp9 (request):
    return render(request,"home/9.html",{})
def temp10 (request):
    return render(request,"home/10.html",{})
