from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Artes(models.Model):

    nombre=models.CharField(max_length=15)
    modelo=models.CharField(max_length=15)
    autor=models.ForeignKey(User,on_delete=models.DO_NOTHING,default=None)
    precio=models.IntegerField()
    def __str__(self):
        return self.autor



class Artistas(models.Model):

    autor=models.ForeignKey(User,on_delete=models.DO_NOTHING,default=None)

    def __str__(self):
        return self.autor

def get_absolute_url(self):
        return reverse('arte_edit', kwargs={'pk': self.pk})
